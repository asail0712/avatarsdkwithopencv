﻿/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
* You may not use this file except in compliance with an authorized license
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
* CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
* See the License for the specific language governing permissions and limitations under the License.
* Written by Itseez3D, Inc. <support@avatarsdk.com>, April 2017
*/

#if UNITY_EDITOR
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace ItSeez3D.AvatarSdk.Core
{
	/// <summary>
	/// Various parameters for creating prefab
	/// </summary>
	public class AvatarPrefabParameters
	{
		private AvatarPrefabParameters() { }

		public AvatarPrefabParameters(string avatarId, string headObjectName)
		{
			this.avatarId = avatarId;
			this.headObjectName = headObjectName;
		}

		/// <summary>
		/// Avatar Id
		/// </summary>
		public string avatarId;

		/// <summary>
		/// Name of the game object with head mesh renderer
		/// </summary>
		public string headObjectName;

		/// <summary>
		/// Head mesh level of details
		/// </summary>
		public int levelOfDetails = 0;

		/// <summary>
		/// Name of the game object with haircut mesh renderer
		/// </summary>
		public string haircutObjectName = null;

		/// <summary>
		/// Haircut Id
		/// </summary>
		public string haircutId = null;

		/// <summary>
		/// Prefab will have blendshapes
		/// </summary>
		public bool withBlendshapes = true;

		/// <summary>
		/// Head and haircut will be merged into a single mesh
		/// </summary>
		public bool mergeHeadAndTextureMeshes = false;

		/// <summary>
		/// Remove all childs of the avatar object
		/// </summary>
		public bool removeChildComponentsForAvatarObject = true;
	}


	public class AvatarPrefabBuilder
	{
		#region signletion staff
		protected static AvatarPrefabBuilder instance = null;

		protected AvatarPrefabBuilder() { }

		public static AvatarPrefabBuilder Instance
		{
			get
			{
				if (instance == null)
					instance = new AvatarPrefabBuilder();
				return instance;
			}
		}
		#endregion

		public void CreateAvatarPrefab(GameObject avatarObject, AvatarPrefabParameters prefabParameters)
		{
			string prefabDir = Path.Combine(PluginStructure.GetPluginDirectoryPath(PluginStructure.PREFABS_DIR, PathOriginOptions.RelativeToAssetsFolder), prefabParameters.avatarId);
			PluginStructure.CreatePluginDirectory(prefabDir);
			GameObject instantiatedAvatar = GameObject.Instantiate(avatarObject);
			GameObject headObject = GetChildByName(instantiatedAvatar, prefabParameters.headObjectName);
			GameObject haircutObject = GetChildByName(instantiatedAvatar, prefabParameters.haircutObjectName);

			if (prefabParameters.mergeHeadAndTextureMeshes)
			{
				SaveMergedMeshAndMaterial(prefabDir, headObject, haircutObject, prefabParameters);
				if (haircutObject != null)
					GameObject.DestroyImmediate(haircutObject);
			}
			else
			{
				SaveMeshAndMaterial(prefabDir, headObject, haircutObject, prefabParameters);
			}

			if (prefabParameters.withBlendshapes)
				CopyBlendshapesWeights(avatarObject, instantiatedAvatar, prefabParameters.headObjectName);

			if (prefabParameters.removeChildComponentsForAvatarObject)
			{
				var childComponents = instantiatedAvatar.GetComponentsInChildren<MonoBehaviour>();
				foreach (var child in childComponents)
					GameObject.DestroyImmediate(child);
			}

			instantiatedAvatar.transform.Rotate(0.0f, 180.0f, 0.0f);

			string prefabPath = prefabDir + "/avatar.prefab";
#if UNITY_2018_3_OR_NEWER
			PrefabUtility.SaveAsPrefabAsset(instantiatedAvatar, prefabPath);
#else
			PrefabUtility.CreatePrefab(prefabPath, instantiatedAvatar);
#endif
			GameObject.DestroyImmediate(instantiatedAvatar);
			EditorUtility.DisplayDialog("Prefab created successfully!", string.Format("You can find your prefab in '{0}' folder", prefabDir), "Ok");
		}

		protected void SaveMeshAndMaterial(string prefabDir, GameObject headObject, GameObject haircutObject, AvatarPrefabParameters prefabParameters)
		{
			if (headObject != null)
			{
				string meshFilePath = Path.Combine(prefabDir, "head.fbx");
				string textureFilePath = CoreTools.GetOutputTextureFilename(meshFilePath);
				SkinnedMeshRenderer headMeshRenderer = headObject.GetComponentInChildren<SkinnedMeshRenderer>();
				headMeshRenderer.sharedMesh = SaveHeadMeshAsFbxAsset(headMeshRenderer, prefabParameters.avatarId, meshFilePath, 
					prefabParameters.withBlendshapes, prefabParameters.levelOfDetails);
				headMeshRenderer.sharedMaterial.mainTexture = LoadTextureAsset(textureFilePath);
				headMeshRenderer.sharedMaterial = InstantiateAndSaveMaterial(headMeshRenderer.sharedMaterial, Path.Combine(prefabDir, "head_material.mat"));

				for (int i = 0; i < headMeshRenderer.sharedMesh.blendShapeCount; i++)
					headMeshRenderer.SetBlendShapeWeight(i, 0.0f);
			}

			if (haircutObject != null && !string.IsNullOrEmpty(prefabParameters.haircutId))
			{
				string haircutMeshFile = Path.Combine(prefabDir, "haircut.fbx");
				string haircutTextureFile = CoreTools.GetOutputTextureFilename(haircutMeshFile);
				MeshRenderer hairMeshRenderer = haircutObject.GetComponentInChildren<MeshRenderer>();
				if (hairMeshRenderer != null)
				{
					haircutObject.GetComponentInChildren<MeshFilter>().mesh = SaveHaircutMeshAsFbxAsset(prefabParameters.avatarId, prefabParameters.haircutId, haircutMeshFile);
					hairMeshRenderer.sharedMaterial.mainTexture = LoadTextureAsset(haircutTextureFile);
					hairMeshRenderer.sharedMaterial = InstantiateAndSaveMaterial(hairMeshRenderer.sharedMaterial, Path.Combine(prefabDir, "haircut_material.mat"));
				}
				else
				{
					SkinnedMeshRenderer hairSkinnedMeshRenderer = haircutObject.GetComponentInChildren<SkinnedMeshRenderer>();
					if (hairSkinnedMeshRenderer != null)
					{
						hairSkinnedMeshRenderer.sharedMesh = SaveHaircutMeshAsFbxAsset(prefabParameters.avatarId, prefabParameters.haircutId, haircutMeshFile);
						hairSkinnedMeshRenderer.sharedMaterial.mainTexture = LoadTextureAsset(haircutTextureFile);
						hairSkinnedMeshRenderer.sharedMaterial = InstantiateAndSaveMaterial(hairSkinnedMeshRenderer.sharedMaterial, Path.Combine(prefabDir, "haircut_material.mat"));
					}
				}
			}

			AssetDatabase.SaveAssets();
		}

		protected void SaveMergedMeshAndMaterial(string prefabDir, GameObject headObject, GameObject haircutObject, AvatarPrefabParameters prefabParameters)
		{
			if (string.IsNullOrEmpty(prefabParameters.haircutId))
			{
				SaveMeshAndMaterial(prefabDir, headObject, haircutObject, prefabParameters);
			}
			else
			{
				Vector4 haircutColor, haircutTint;
				GetHaircutColorAndTint(haircutObject, out haircutColor, out haircutTint);

				string meshFilePath = Path.Combine(prefabDir, "head_with_haircut.fbx");
				string textureFilePath = CoreTools.GetOutputTextureFilename(meshFilePath);
				SkinnedMeshRenderer headMeshRenderer = headObject.GetComponentInChildren<SkinnedMeshRenderer>();
				headMeshRenderer.sharedMesh = SaveAvatarMergedMeshAsFbxAsset(headMeshRenderer, prefabParameters.avatarId, prefabParameters.haircutId, meshFilePath, 
					haircutColor, haircutTint, prefabParameters.withBlendshapes, prefabParameters.levelOfDetails);
				headMeshRenderer.sharedMaterial = new Material(Shader.Find("AvatarUnlitShader"));
				headMeshRenderer.sharedMaterial.mainTexture = LoadTextureAsset(textureFilePath);
				headMeshRenderer.sharedMaterial = InstantiateAndSaveMaterial(headMeshRenderer.sharedMaterial, Path.Combine(prefabDir, "avatar_material.mat"));
			}

			AssetDatabase.SaveAssets();
		}

		protected void GetHaircutColorAndTint(GameObject haircutObject, out Vector4 haircutColor, out Vector4 haircutTint)
		{
			Renderer hairMeshRenderer = haircutObject.GetComponent<Renderer>();
			haircutColor = hairMeshRenderer.sharedMaterial.GetVector("_ColorTarget");
			haircutTint = hairMeshRenderer.sharedMaterial.GetVector("_ColorTint");
		}

		protected Material InstantiateAndSaveMaterial(Material material, string assetPath)
		{
			Material instanceMat = GameObject.Instantiate(material);
			AssetDatabase.CreateAsset(instanceMat, assetPath);
			return instanceMat;
		}

		protected Texture2D LoadTextureAsset(string texturePath)
		{
			return (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(Texture2D));
		}

		protected Texture2D SaveTextureAsset(Texture texture, string texturePath)
		{
			Texture2D texture2D = texture as Texture2D;
			if (texture2D.format == TextureFormat.DXT1 || texture2D.format == TextureFormat.DXT5)
			{
				//It is compressed texture, so it is already saved somewhere
				return texture2D;
			}

			byte[] textureBytes = null;
			if (texturePath.ToLower().EndsWith(".png"))
			{
				textureBytes = texture2D.EncodeToPNG();
			}
			else if (texturePath.ToLower().EndsWith(".jpg"))
			{
				textureBytes = texture2D.EncodeToJPG();
			}
			else
			{
				Debug.LogErrorFormat("Unsupported texture format: {0}", texturePath);
				return texture2D;
			}
			File.WriteAllBytes(texturePath, textureBytes);
			AssetDatabase.Refresh();

			TextureImporter textureImporter = (TextureImporter)TextureImporter.GetAtPath(texturePath);
			textureImporter.isReadable = true;
			textureImporter.SaveAndReimport();

			return LoadTextureAsset(texturePath);
		}

		protected Mesh SaveAvatarMergedMeshAsFbxAsset(SkinnedMeshRenderer headMeshRenderer, string avatarId, string haircutId, string fbxPath, 
			Color haircutColor, Vector4 haircutTint, bool withBlendshapes, int levelOfDetails)
		{
			CoreTools.SaveAvatarMesh(headMeshRenderer, avatarId, fbxPath, MeshFileFormat.FBX, !withBlendshapes, withBlendshapes, haircutId, haircutColor, haircutTint, levelOfDetails);
			AssetDatabase.Refresh();

			ModelImporter modelImporter = AssetImporter.GetAtPath(fbxPath) as ModelImporter;
			modelImporter.isReadable = true;
			modelImporter.normalCalculationMode = ModelImporterNormalCalculationMode.Unweighted;
			modelImporter.SaveAndReimport();

			Mesh mesh = (Mesh)AssetDatabase.LoadAssetAtPath(fbxPath, typeof(Mesh));
			return mesh;
		}

		protected Mesh SaveHeadMeshAsFbxAsset(SkinnedMeshRenderer headMeshRenderer, string avatarId, string fbxPath, bool withBlendshapes, int levelOfDetails)
		{
			CoreTools.SaveAvatarMesh(headMeshRenderer, avatarId, fbxPath, MeshFileFormat.FBX, !withBlendshapes, withBlendshapes, levelOfDetails: levelOfDetails);
			AssetDatabase.Refresh();

			ModelImporter modelImporter = AssetImporter.GetAtPath(fbxPath) as ModelImporter;
			modelImporter.isReadable = true;
			modelImporter.normalCalculationMode = ModelImporterNormalCalculationMode.Unweighted;
			modelImporter.SaveAndReimport();

			Mesh mesh = (Mesh)AssetDatabase.LoadAssetAtPath(fbxPath, typeof(Mesh));
			return mesh;
		}

		protected Mesh SaveHaircutMeshAsFbxAsset(string avatarId, string haircutId, string fbxPath)
		{
			CoreTools.HaircutPlyToFbx(avatarId, haircutId, fbxPath);
			AssetDatabase.Refresh();

			ModelImporter modelImporter = ModelImporter.GetAtPath(fbxPath) as ModelImporter;
			modelImporter.isReadable = true;
			modelImporter.SaveAndReimport();

			Mesh mesh = (Mesh)AssetDatabase.LoadAssetAtPath(fbxPath, typeof(Mesh));
			return mesh;
		}

		protected GameObject GetChildByName (GameObject obj, string name)
		{
			var children = obj.GetComponentsInChildren<Transform> ();
			foreach (var child in children) {
				if (child.name.ToLower () == name.ToLower ())
					return child.gameObject;
			}

			return null;
		}

		protected void CopyBlendshapesWeights(GameObject srcAvatarObject, GameObject dstAvatarObject, string headObjectName)
		{
			GameObject srcAvatarHead = GetChildByName(srcAvatarObject, headObjectName);
			GameObject dstAvatarHead = GetChildByName(dstAvatarObject, headObjectName);

			SkinnedMeshRenderer srcMeshRenderer = srcAvatarHead.GetComponentInChildren<SkinnedMeshRenderer>();
			SkinnedMeshRenderer dstMeshRenderer = dstAvatarHead.GetComponentInChildren<SkinnedMeshRenderer>();

			for (int i = 0; i < dstMeshRenderer.sharedMesh.blendShapeCount; i++)
				dstMeshRenderer.SetBlendShapeWeight(i, 0.0f);

			for (int i=0; i<srcMeshRenderer.sharedMesh.blendShapeCount; i++)
			{
				string blendshapeName = srcMeshRenderer.sharedMesh.GetBlendShapeName(i);
				int idx = dstMeshRenderer.sharedMesh.GetBlendShapeIndex(blendshapeName);
				if (idx >= 0)
				{
					dstMeshRenderer.SetBlendShapeWeight(idx, srcMeshRenderer.GetBlendShapeWeight(i));
				}
			}
		}
	}
}
#endif
