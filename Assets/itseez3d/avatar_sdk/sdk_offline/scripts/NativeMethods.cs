﻿/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
* You may not use this file except in compliance with an authorized license
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
* CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
* See the License for the specific language governing permissions and limitations under the License.
* Written by Itseez3D, Inc. <support@avatarsdk.com>, April 2017
*/

using ItSeez3D.AvatarSdk.Core;
using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace ItSeez3D.AvatarSdk.Offline
{
	public static class PipelineTypeExtensionsOffline
	{
		/// <summary>
		/// Cast PipelineType enum value to SDK's AvatarSdkPipelineType value
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static AvatarSdkPipelineType ToSdkPipelineType(this PipelineType type)
		{
			switch(type)
			{
				case PipelineType.FACE:
				case PipelineType.STYLED_FACE:
					return AvatarSdkPipelineType.AVATAR_SDK_PIPELINE_FACE;
				case PipelineType.HEAD:
					return AvatarSdkPipelineType.AVATAR_SDK_PIPELINE_HEAD;
				default:
					return AvatarSdkPipelineType.AVATAR_SDK_PIPELINE_UNKNOWN;
			}
		}
	}
	delegate void ReportProgress(float progressFraction);

	public enum AvatarSdkPipelineType
	{
		AVATAR_SDK_PIPELINE_UNKNOWN = 0,   ///< Unknown pipeline type
		AVATAR_SDK_PIPELINE_FACE = 1,      ///< Face avatar
		AVATAR_SDK_PIPELINE_HEAD = 2       ///< Head a         qvatar
	}

	public enum AvatarSdkMeshFormat
	{
		AVATAR_SDK_MESH_FORMAT_PLY,
		AVATAR_SDK_MESH_FORMAT_OBJ
	};

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
	struct AvatarSdkParams
	{
		public AvatarSdkPipelineType pipelineType;
		[MarshalAs(UnmanagedType.LPStr)]
		public string inputImagePath;
		public IntPtr wInputImagePath;
		[MarshalAs(UnmanagedType.LPStr)]
		public string outputDirPath;
		[MarshalAs(UnmanagedType.LPStr)]
		public string resourcesJson;
		[MarshalAs(UnmanagedType.LPStr)]
		public string resourcesJsonFilePath;
		public AvatarSdkMeshFormat outputMeshFormat;
		[MarshalAs(UnmanagedType.U1)]
		public bool saveHaircutsAsPointClouds;
		public IntPtr modelInfo;
        public IntPtr avatarModification;
		public IntPtr additionalTextures;

		public IntPtr haircuts;
		public int haircutsNumber;

		public IntPtr blendshapes;
		public int blendshapesNumber;
	}

	class NativeMethods
	{
		#region Dll interface

		[DllImport(DllHelper.dll)]
		private static extern int initAvatarSdk(string programName, string resourcesPath);

		[DllImport(DllHelper.dll)]
		private static extern int releaseAvatarSdk();

		[DllImport(DllHelper.dll)]
		private unsafe static extern void getLastError(byte* buffer, int size);

		[DllImport(DllHelper.dll)]
		private static extern int initializeAvatarFromRawData(
			IntPtr rawPhotoBytesRGBA,
			int w,
			int h,
			string avatarDirectory
		);

		[DllImport(DllHelper.dll)]
		private static extern int generateUnityAvatar(AvatarSdkPipelineType pipelineType, string inputImagePath, string outputDirPath, string resourcesJson, ReportProgress reportProgressFunc);

		[DllImport(DllHelper.dll)]
		private static extern int generateAdditionalHaircutsFromJson(string meshFilePath, string outputDir, string resourcesJsonWithHaircuts, AvatarSdkMeshFormat format, bool saveAsPointCloud);

		[DllImport(DllHelper.dll)]
		private static extern int generateLODMesh(AvatarSdkPipelineType pipelineType, int levelOfDetails, string meshFilePath, string outMeshFilePath, string blendshapesDir, string outBlendshapesDir);

		[DllImport(DllHelper.dll)]
		private static extern int extractHaircutFromResources(string haircutId, string haircutsDirectory);

		[DllImport(DllHelper.dll)]
		private static extern int extractHaircutPreviewFromResources(string haircutId, string haircutsDirectory);

		[DllImport(DllHelper.dll)] 
		private static extern void setResourcesPath(AvatarSdkPipelineType pipelineType, string resourcesPath);

		[DllImport(DllHelper.dll)]
		[return: MarshalAs(UnmanagedType.I1)]
		private static extern bool isHardwareSupported();

		[DllImport(DllHelper.dll)]
		private static extern void setLoggingFile(string logFile);

		[DllImport(DllHelper.dll)]
		private static extern void setAdvancedLogs(bool isAdvanced);


		#endregion

		#region public methods
		public static int InitAvatarSdk(string programName, string resourcesPath = "")
		{
			setAdvancedLogs(true);
			setLoggingFile(Path.Combine(Application.persistentDataPath, "avatar_sdk.log"));

			var code = initAvatarSdk(programName, resourcesPath);
			return code;
		}

		public static void ReleaseAvatarSdk()
		{
			releaseAvatarSdk();
		}

		public unsafe static void GetLastError(byte* buffer, int size)
		{
			getLastError(buffer, size);  
		}

		public static int InitializeAvatarFromRawData(IntPtr rawPhotoBytesRGBA, int w, int h, string avatarDirectory)
		{
			return initializeAvatarFromRawData(rawPhotoBytesRGBA, w, h, avatarDirectory);
		}

		public static int GenerateAvatar(AvatarSdkPipelineType pipelineType, string inputImagePath, string outputDirPath, string resourcesJson, ReportProgress reportProgressFunc)
		{
			return generateUnityAvatar(pipelineType, inputImagePath, outputDirPath, resourcesJson, reportProgressFunc);
		}

		public static int GenerateAdditionalHaircuts(string meshFilePath, string outputDir, string resourcesJsonWithHaircuts)
		{
			return generateAdditionalHaircutsFromJson(meshFilePath, outputDir, resourcesJsonWithHaircuts, AvatarSdkMeshFormat.AVATAR_SDK_MESH_FORMAT_PLY, true);
		}

		public static int GenerateLODMesh(AvatarSdkPipelineType pipelineType, int levelOfDetails, string meshFilePath, string outMeshFilePath, string blendshapesDir, string outBlendshapesDir)
		{
			return generateLODMesh(pipelineType, levelOfDetails, meshFilePath, outMeshFilePath, blendshapesDir, outBlendshapesDir);
		}

		public static int ExtractHaircutFromResources(string haircutId, string haircutsDirectory)
		{
			return extractHaircutFromResources(haircutId, haircutsDirectory);
		}

		public static int ExtractHaircutPreviewFromResources(string haircutId, string haircutsDirectory)
		{
			return extractHaircutPreviewFromResources(haircutId, haircutsDirectory);
		}

		public static void SetResourcesPath(AvatarSdkPipelineType pipelineType, string resourcesPath)
		{
			setResourcesPath(pipelineType, resourcesPath);
		}

		public static bool IsHardwareSupported()
		{
			return isHardwareSupported();
		}
		#endregion
	}
}
